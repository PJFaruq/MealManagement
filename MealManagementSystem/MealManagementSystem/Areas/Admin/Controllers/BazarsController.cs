﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MealManagementSystem.Models;

namespace MealManagementSystem.Areas.Admin.Controllers
{
    public class BazarsController : Controller
    {
        private MealDBContex db = new MealDBContex();

        // GET: Admin/Bazars
        public ActionResult Index()
        {
            var bazars = db.Bazars.Include(b => b.User);
            return View(bazars.ToList());
        }

        // GET: Admin/Bazars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bazar bazar = db.Bazars.Find(id);
            if (bazar == null)
            {
                return HttpNotFound();
            }
            return View(bazar);
        }

        // GET: Admin/Bazars/Create
        public ActionResult Create()
        {
            ViewBag.MemberId = new SelectList(db.Users.Where(model=>model.UserType=="Member"), "UserId", "UserName");
            return View();
        }

        // POST: Admin/Bazars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BazarId,MemberId,Date,Amount,Note")] Bazar bazar)
        {
            if (ModelState.IsValid)
            {
                db.Bazars.Add(bazar);
                db.SaveChanges();
                ViewBag.SuccessMsg = "Bazar Added Successfully...";
                ViewBag.MemberId = new SelectList(db.Users.Where(model => model.UserType == "Member"), "UserId", "UserName");
                ModelState.Clear();
                return View();
            }

            ViewBag.MemberId = new SelectList(db.Users.Where(model=>model.UserType=="Member"), "UserId", "UserName", bazar.MemberId);
            return View(bazar);
        }

        // GET: Admin/Bazars/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bazar bazar = db.Bazars.Find(id);
            if (bazar == null)
            {
                return HttpNotFound();
            }
            ViewBag.MemberId = new SelectList(db.Users, "UserId", "UserName", bazar.MemberId);
            return View(bazar);
        }

        // POST: Admin/Bazars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BazarId,MemberId,Date,Amount,Note")] Bazar bazar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bazar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MemberId = new SelectList(db.Users, "UserId", "UserName", bazar.MemberId);
            return View(bazar);
        }

        // GET: Admin/Bazars/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bazar bazar = db.Bazars.Find(id);
            if (bazar == null)
            {
                return HttpNotFound();
            }
            return View(bazar);
        }

        // POST: Admin/Bazars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bazar bazar = db.Bazars.Find(id);
            db.Bazars.Remove(bazar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
