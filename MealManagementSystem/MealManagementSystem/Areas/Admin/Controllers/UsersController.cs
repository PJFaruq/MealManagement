﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MealManagementSystem.Models;
using MealManagementSystem.Areas.Admin.Models;

namespace MealManagementSystem.Areas.Admin.Controllers
{
    public class UsersController : Controller
    {
        private MealDBContex db = new MealDBContex();

        // GET: Admin/Users
        public ActionResult ViewAllMember()
        {
            return View(db.Users.Where(model=>model.UserType=="Member").ToList());
        }

        // GET: Admin/Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserId,UserName,Password,FullName,UserType")] User user)
        {
            user.UserType = "Member";
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        public JsonResult CheckUserName(string UserName)
        {
            return Json(!db.Users.Any(model => model.UserName == UserName), JsonRequestBehavior.AllowGet);
        }

        // GET: Admin/Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Admin/Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserId,UserName,Password,FullName,UserType")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Admin/Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Admin/Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            List<Bazar> bazar = db.Bazars.Where(model => model.MemberId == id).ToList();
            db.Bazars.RemoveRange(bazar);
            List<Meal> meal = db.Meals.Where(model => model.MemberId == id).ToList();
            db.Meals.RemoveRange(meal);

            List<Payment> payment = db.Payments.Where(model => model.MemberId == id).ToList();
            db.Payments.RemoveRange(payment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ViewDetail()
        {
            List<DetailsViewModel> listofDetail = new List<DetailsViewModel>();

            double totalMealSpent = 0;
            double totalMeal = 0;
            double totalExtraSpent = 0;
            double mealRate = 0;
            double ExtraSpentPerMember;

            int totalNumberofMember = db.Users.Count(model => model.UserType == "Member");
            if (totalNumberofMember > 0)
            {
                //Calculating Total Number of Meal
                List<double> totalListofMeal = db.Meals.Select(model => model.Total).ToList();
                foreach (double item in totalListofMeal)
                {
                    totalMeal = totalMeal + item;
                }

                //Calculating Total Meal Spent
                List<double> totalListofMealSpent = db.Bazars.Select(model => model.Amount).ToList();
                foreach (double item in totalListofMealSpent)
                {
                    totalMealSpent = totalMealSpent + item;
                }

                //Calculating Total Extra Spent
                List<double> totalListofExtraSpent = db.Extras.Select(model => model.Amount).ToList();
                foreach (double item in totalListofExtraSpent)
                {
                    totalExtraSpent = totalExtraSpent + item;
                }

                //Calculating Extra Spent Per Member
                ExtraSpentPerMember = totalExtraSpent / totalNumberofMember;

                //Calculating Meal Rate
                if (totalMealSpent > 0 && totalMeal > 0)
                {
                    mealRate = totalMealSpent / totalMeal;
                }


                List<int> listofMemberId = db.Users.Where(model => model.UserType == "Member").Select(model => model.UserId).ToList();
                foreach (int userId in listofMemberId)
                {
                    User user = db.Users.Where(model => model.UserId == userId).FirstOrDefault();
                    string name = user.FullName;
                    double totalNumberofMeal = 0;
                    List<double> numberofMeal = db.Meals.Where(model => model.MemberId == userId).Select(model => model.Total).ToList();
                    foreach (double item in numberofMeal)
                    {
                        totalNumberofMeal = totalNumberofMeal + item;
                    }

                    double mealSpent = totalNumberofMeal * mealRate;
                    double totalSpent = mealSpent + ExtraSpentPerMember;

                    Payment payment = db.Payments.Where(model => model.MemberId == userId).FirstOrDefault();
                    double receivable = 0;
                    double payable = 0;
                    double paidAmount = 0;
                    if (payment != null)
                    {
                        paidAmount = payment.Amount;
                    }


                    double remaining = paidAmount - totalSpent;
                    if (remaining > 0)
                    {
                        receivable = 0;
                        payable = remaining;
                    }
                    else if (remaining < 0)
                    {
                        receivable = -(remaining);
                        payable = 0;
                    }
                    else
                    {
                        receivable = 0;
                        payable = 0;
                    }



                    DetailsViewModel detail = new DetailsViewModel();
                    detail.Name = name;
                    detail.NumberOfMeal = totalNumberofMeal;
                    detail.MealRate = mealRate;
                    detail.MealSpent = mealSpent;
                    detail.ExtraSpent = ExtraSpentPerMember;
                    detail.TotalSpent = totalSpent;
                    detail.Paid = paidAmount;
                    detail.Receivable = receivable;
                    detail.Payable = payable;

                    listofDetail.Add(detail);

                }
            }


            return View(listofDetail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
