﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MealManagementSystem.Models;

namespace MealManagementSystem.Areas.Admin.Controllers
{
    public class PaymentsController : Controller
    {
        private MealDBContex db = new MealDBContex();

        // GET: Admin/Payments
        public ActionResult Index()
        {
            var payments = db.Payments.Include(p => p.User);
            return View(payments.ToList());
        }

        // GET: Admin/Payments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment payment = db.Payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            return View(payment);
        }

        // GET: Admin/Payments/Create
        public ActionResult Create()
        {
            ViewBag.MemberId = new SelectList(db.Users.Where(model=>model.UserType=="Member"), "UserId", "UserName");
            return View();
        }

        // POST: Admin/Payments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PaymentId,MemberId,Date,Amount,Note")] Payment payment)
        {
            var paymentFromDB = db.Payments.FirstOrDefault(model=>model.MemberId==payment.MemberId);
            
            if (paymentFromDB == null)
            {
                if (ModelState.IsValid)
                {
                    db.Payments.Add(payment);
                    db.SaveChanges();
                    ViewBag.SuccessMsg = "Payment Added Successfully.....";
                    ModelState.Clear();
                    ViewBag.MemberId = new SelectList(db.Users.Where(model => model.UserType == "Member"), "UserId", "UserName");
                    return View();
                }
            }
            else
            {
                payment.Note = paymentFromDB.Note + " | " + payment.Note;
                payment.Amount = paymentFromDB.Amount + payment.Amount;
                payment.PaymentId = paymentFromDB.PaymentId;
                
                if (ModelState.IsValid)
                {
                    db.Entry(paymentFromDB).State = EntityState.Detached;
                    db.Entry(payment).State = EntityState.Modified;
                    db.SaveChanges();
                    ViewBag.SuccessMsg = "Payment Added Successfully......";
                    ModelState.Clear();
                    ViewBag.MemberId = new SelectList(db.Users.Where(model => model.UserType == "Member"), "UserId", "UserName");
                    return View();
                }
            }
            

            ViewBag.MemberId = new SelectList(db.Users.Where(model => model.UserType == "Member"), "UserId", "UserName",payment.MemberId);
            return View(payment);
        }

        // GET: Admin/Payments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment payment = db.Payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            ViewBag.MemberId = new SelectList(db.Users, "UserId", "UserName", payment.MemberId);
            return View(payment);
        }

        // POST: Admin/Payments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PaymentId,MemberId,Date,Amount,Note")] Payment payment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MemberId = new SelectList(db.Users, "UserId", "UserName", payment.MemberId);
            return View(payment);
        }

        // GET: Admin/Payments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment payment = db.Payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            return View(payment);
        }

        // POST: Admin/Payments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Payment payment = db.Payments.Find(id);
            db.Payments.Remove(payment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
