﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MealManagementSystem.Models;

namespace MealManagementSystem.Areas.Admin.Controllers
{
    public class MealsController : Controller
    {
        private MealDBContex db = new MealDBContex();

        //View Meal By Date
        public ActionResult ViewMeal()
        {

            return View();
        }
        [HttpPost]
        public ActionResult ViewMeal(DateTime Date)
        {
            
            return View(db.Meals.Where(model=>model.Date==Date).ToList());
        }

        // GET: Admin/Meals
        public ActionResult Index()
        {
            var meals = db.Meals.Include(m => m.User);
            return View(meals.ToList());
        }

        // GET: Admin/Meals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meal meal = db.Meals.Find(id);
            if (meal == null)
            {
                return HttpNotFound();
            }
            return View(meal);
        }

        // GET: Admin/Meals/Create
        public ActionResult Create()
        {


            ViewBag.MemberId = new SelectList(db.Users.Where(model=>model.UserType=="Member"), "UserId", "UserName");
            return View();
        }

        // POST: Admin/Meals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MealId,MemberId,Date,Breakfast,Lunch,Dinner")] Meal meal)
        {
            meal.Total = meal.Breakfast + meal.Lunch + meal.Dinner;
            if (db.Meals.Any(model => model.Date == meal.Date && model.MemberId==meal.MemberId))
            {
                ModelState.AddModelError("Date", "Meal is already given for this Date ");
            }
            if (ModelState.IsValid)
            {
                db.Meals.Add(meal);
                db.SaveChanges();
                ViewBag.SuccessMsg = "Meal Saved Successfully....";
                ModelState.Clear();
                ViewBag.MemberId = new SelectList(db.Users.Where(model => model.UserType == "Member"), "UserId", "UserName");
                return View();
            }

            ViewBag.MemberId = new SelectList(db.Users.Where(model=>model.UserType=="Member"), "UserId", "UserName", meal.MemberId);
            return View(meal);
        }



        // GET: Admin/Meals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meal meal = db.Meals.Find(id);
            if (meal == null)
            {
                return HttpNotFound();
            }
            ViewBag.MemberId = new SelectList(db.Users, "UserId", "UserName", meal.MemberId);
            return View(meal);
        }

        // POST: Admin/Meals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MealId,MemberId,Date,Breakfast,Lunch,Dinner,Total")] Meal meal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(meal).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MemberId = new SelectList(db.Users, "UserId", "UserName", meal.MemberId);
            return View(meal);
        }

        // GET: Admin/Meals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meal meal = db.Meals.Find(id);
            if (meal == null)
            {
                return HttpNotFound();
            }
            return View(meal);
        }

        // POST: Admin/Meals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Meal meal = db.Meals.Find(id);
            db.Meals.Remove(meal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
