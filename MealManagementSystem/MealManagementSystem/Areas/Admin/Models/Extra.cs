﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MealManagementSystem.Models
{
    [MetadataType(typeof(ExtraMetadata))]
    public partial class Extra
    {
    }
    public class ExtraMetadata
    {
        [Required]
        [DataType(DataType.MultilineText)]
        [StringLength(maximumLength:200,ErrorMessage ="Maximum 200 Character are allowed")]
        public string Reason { get; set; }

        [Required]
        public double Amount { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public System.DateTime Date { get; set; }
    }
}