﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MealManagementSystem.Models
{
   [ MetadataType(typeof(MealMetaData))]
    public partial class Meal
    {
    }

    public class MealMetaData
    {
        [Required]
        [Display(Name ="Member Name")]
        public int MemberId { get; set; }
    }
}