﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MealManagementSystem.Models
{
    [MetadataType(typeof(BazarMetadata))]
    public partial class Bazar
    {
    }

    public class BazarMetadata
    {
        [Required]
        [Display(Name = "Member Name")]
        public int MemberId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public System.DateTime Date { get; set; }

        [Required]
        public double Amount { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(maximumLength:200,ErrorMessage ="Maximum 200 Character are allowed")]
        public string Note { get; set; }
    }
}