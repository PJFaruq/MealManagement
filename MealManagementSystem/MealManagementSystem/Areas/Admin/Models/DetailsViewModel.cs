﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MealManagementSystem.Areas.Admin.Models
{
    public class DetailsViewModel
    {
        [Key]
        public int DetailsId { get; set; }
        public string Name { get; set; }

        [Display(Name="Total Meal")]
        public double NumberOfMeal { get; set; }

        [Display(Name = "Meal Spent")]
        public double MealSpent { get; set; }

        [Display(Name = "Extra Spent")]
        public double ExtraSpent { get; set; }

        [Display(Name = "Total Spent")]
        public double TotalSpent { get; set; }
        public double Paid { get; set; }
        public double Payable { get; set; }
        public double Receivable { get; set; }

        [Display(Name = "Meal Rate")]
        public double MealRate { get; set; }

    }
}