﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MealManagementSystem.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
    }

    public class UserMetadata
    {
        [Required]
        [Display(Name ="Username")]
        [StringLength(maximumLength:20,ErrorMessage ="The length should be in between 4 to 20 Character",MinimumLength =4)]
        [Remote("CheckUserName","Users",ErrorMessage ="This Username is already Exist")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(maximumLength: 20, ErrorMessage = "The length should be in between 4 to 20 Character", MinimumLength = 4)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        [StringLength(maximumLength: 50, ErrorMessage = "The length should be in between 4 to 20 Character", MinimumLength = 4)]
        public string FullName { get; set; }
    }
}